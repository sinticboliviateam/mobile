import 'package:escarp_app/share_prefs/preferencias_usuarios.dart';
import 'package:flutter/material.dart';

class DrawerWidget extends StatefulWidget {
  final prefs = new PreferenciasUsuario();
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  final prefs = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
          color: Color.fromRGBO(33, 41, 74, 1.0),
          child: ListView(
            padding: EdgeInsets.all(10.0),
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              Container(
                alignment: Alignment.centerLeft,
                height: 50.0,
                width: 50.0,
                child: Image.asset('assets/images/icono_app.png'),
              ),
              SizedBox(
                height: 20,
              ),
              Divider(color: Color.fromRGBO(243, 250, 175, 1.0)),
              ListTile(
                leading: Icon(Icons.book_online_outlined,
                    color: Color.fromRGBO(16, 216, 200, 1.0)),
                title: _idiomaLibro(),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, 'libro');
                },
              ),
              Divider(color: Color.fromRGBO(243, 250, 175, 1.0)),
              ListTile(
                // trailing: SwitchListTile(value: true, onChanged: (value) {}),
                leading: Icon(Icons.language_outlined,
                    color: Color.fromRGBO(16, 216, 200, 1.0)),
                title: _idiomaLenguaje(),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, 'idioma');
                },
              ),
              // Divider(color: Color.fromRGBO(243, 250, 175, 1.0)),
              // ListTile(
              //   leading: Icon(Icons.people_outline_outlined,
              //       color: Color.fromRGBO(16, 216, 200, 1.0)),
              //   title: _idiomaEquipoEscArp(),
              //   onTap: () {},
              // ),
              Divider(color: Color.fromRGBO(243, 250, 175, 1.0)),
              ListTile(
                leading: Image.asset(
                  'assets/images/icono_mem.png',
                  scale: 1.8,
                ),
                title: _idiomaVersion(),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, 'version');
                },
              ),
              Divider(color: Color.fromRGBO(243, 250, 175, 1.0)),
            ],
          )),
    );
  }

  Widget _idiomaLibro() {
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text('Libro Escarp',
          style: TextStyle(
              fontSize: 20.0,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'GillSans'));
    } else {
      return Text('EscArp Book',
          style: TextStyle(
              fontSize: 20.0,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'GillSans'));
    }
  }

  Widget _idiomaLenguaje() {
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text('Idioma',
          style: TextStyle(
              fontSize: 20.0,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'GillSans'));
    } else {
      return Text('Language',
          style: TextStyle(
              fontSize: 20.0,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'GillSans'));
    }
  }

  // Widget _idiomaEquipoEscArp() {
  //   final prefs = new PreferenciasUsuario();
  //   if (prefs.idioma == true) {
  //     return Text('Equipo EscArp',
  //         style: TextStyle(
  //             fontSize: 20.0,
  //             color: Color.fromRGBO(243, 250, 175, 1.0),
  //             fontFamily: 'GillSans'));
  //   } else {
  //     return Text('EscArp Team',
  //         style: TextStyle(
  //             fontSize: 20.0,
  //             color: Color.fromRGBO(243, 250, 175, 1.0),
  //             fontFamily: 'GillSans'));
  //   }
  // }

  Widget _idiomaVersion() {
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text('Acerca de',
          style: TextStyle(
              fontSize: 20.0,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'GillSans'));
    } else {
      return Text('About',
          style: TextStyle(
              fontSize: 20.0,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'GillSans'));
    }
  }
}
