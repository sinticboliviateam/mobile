import 'package:flutter/material.dart';
import 'package:native_video_view/native_video_view.dart';
import 'package:escarp_app/share_prefs/preferencias_usuarios.dart';

class TutorialPage extends StatefulWidget {
  final String nombre;

  TutorialPage({this.nombre});

  @override
  _TutorialPageState createState() => _TutorialPageState();
}

class _TutorialPageState extends State<TutorialPage> {
  final prefs = new PreferenciasUsuario();
  VideoViewController _controller;
  // bool _isPlaying = false;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        // appBar: AppBar(
        //   // leading: FlatButton(
        //   //   onPressed: () {
        //   //     Navigator.pop(context);
        //   //   },
        //   //   child: Icon(
        //   //     Icons.keyboard_backspace_rounded,
        //   //     color: Color.fromRGBO(16, 216, 200, 1.0),
        //   //     size: size.height * 0.1,
        //   //   ),
        //   // ),
        //   elevation: 0.0,
        //   iconTheme: IconThemeData(color: Color.fromRGBO(16, 216, 200, 1.0)),
        //   toolbarHeight: size.height * 0.1,
        //   backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
        //   // centerTitle: true,
        //   // title: Text(
        //   //   'EscArp',
        //   //   style: TextStyle(
        //   //     fontSize: size.height * 0.10,
        //   //     fontFamily: 'Autography',
        //   //     fontWeight: FontWeight.w900,
        //   //     color: Color.fromRGBO(16, 216, 200, 1.0),
        //   //   ),
        //   // ),
        // ),
        backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
        body: _videoTutorial(),
        floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        floatingActionButton: Container(
          width: size.height * 0.1,
          height: size.height * 0.1,
          child: FloatingActionButton(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.keyboard_backspace_rounded,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              size: size.height * 0.1,
            ),
          ),
        ));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  Widget _videoTutorial() {
    final prefs = new PreferenciasUsuario();
    final size = MediaQuery.of(context).size;

    if (prefs.idioma == true) {
      return Center(
        child: Container(
          alignment: Alignment.center,
          width: size.width * 0.7,
          // child: AspectRatio(
          // aspectRatio: 4 / 3,
          child: NativeVideoView(
            keepAspectRatio: true,
            showMediaController: true,
            enableVolumeControl: true,
            onCreated: (controller) {
              controller.setVideoSource(
                'assets/videos/tutorial_espanol.mp4',
                sourceType: VideoSourceType.asset,
                requestAudioFocus: true,
              );

              setState(() {
                _controller = controller;
              });
            },
            onPrepared: (controller, info) {
              controller.play();
              // controller.pause();
            },
            onError: (controller, what, extra, message) {
              print('Player Error ($what | $extra | $message)');
            },
            onCompletion: (controller) {
              print('Video completed');
            },
          ),
        ),
      );
    } else {
      return Center(
        child: Container(
          alignment: Alignment.center,
          width: size.width * 0.7,
          // child: AspectRatio(
          // aspectRatio: 4 / 3,
          child: NativeVideoView(
            keepAspectRatio: true,
            showMediaController: true,
            enableVolumeControl: true,
            onCreated: (controller) {
              controller.setVideoSource(
                'assets/videos/tutorial_ingles.mp4',
                sourceType: VideoSourceType.asset,
                requestAudioFocus: true,
              );

              setState(() {
                _controller = controller;
              });
            },
            onPrepared: (controller, info) {
              controller.play();
              // controller.pause();
            },
            onError: (controller, what, extra, message) {
              print('Player Error ($what | $extra | $message)');
            },
            onCompletion: (controller) {
              print('Video completed');
            },
          ),
        ),
      );
    }
  }
}
