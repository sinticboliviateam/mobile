import 'package:escarp_app/share_prefs/preferencias_usuarios.dart';
import 'package:flutter/material.dart';

class DesafioEscArp extends StatefulWidget {
  @override
  _DesafioEscArpState createState() => _DesafioEscArpState();
}

class _DesafioEscArpState extends State<DesafioEscArp> {
  final prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
      appBar: AppBar(
          leading: FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.keyboard_backspace_rounded,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              size: size.height * 0.1,
            ),
          ),
          elevation: 0.0,
          iconTheme: IconThemeData(color: Color.fromRGBO(16, 216, 200, 1.0)),
          toolbarHeight: size.height * 0.2,
          backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
          centerTitle: true,
          title: _textoTitulo()),
      body: Center(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                color: Color.fromRGBO(243, 250, 175, 1.0),
              ),
              width: size.width * 0.9,
              height: 5,
            ),
            Expanded(child: Container()),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(),
                  ),
                  Container(
                    width: size.width * 0.2,
                    height: size.width * 0.2,
                    child: RaisedButton(
                        elevation: 10.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                            side: BorderSide(
                              color: Color.fromRGBO(16, 216, 200, 1.0),
                              width: 7.0,
                            )),
                        color: Color.fromRGBO(33, 41, 74, 1.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: size.height * 0.06),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    width: size.height * 0.09,
                                    height: size.height * 0.09,
                                    child: Image.asset(
                                        'assets/images/estrella.png')),
                              ],
                            ),
                            Expanded(child: Container()),
                            _botonFacil(),
                            SizedBox(height: size.height * 0.08),
                          ],
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, 'desafioescarpbasico');
                        }),
                  ),
                  Expanded(child: Container()),
                  Container(
                    width: size.width * 0.2,
                    height: size.width * 0.2,
                    child: RaisedButton(
                        elevation: 10.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                            side: BorderSide(
                              color: Color.fromRGBO(16, 216, 200, 1.0),
                              width: 7.0,
                            )),
                        color: Color.fromRGBO(33, 41, 74, 1.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: size.height * 0.06),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(child: Container()),
                                Container(
                                    width: size.height * 0.09,
                                    height: size.height * 0.09,
                                    child: Image.asset(
                                        'assets/images/estrella.png')),
                                Expanded(child: Container()),
                                Container(
                                    width: size.height * 0.09,
                                    height: size.height * 0.09,
                                    child: Image.asset(
                                        'assets/images/estrella.png')),
                                Expanded(child: Container()),
                              ],
                            ),
                            Expanded(child: Container()),
                            _botonMedio(),
                            SizedBox(height: size.height * 0.08),
                          ],
                        ),
                        onPressed: () {
                          Navigator.pushNamed(
                              context, 'desafioescarpintermedio');
                        }),
                  ),
                  Expanded(child: Container()),
                  Container(
                    width: size.width * 0.2,
                    height: size.width * 0.2,
                    child: RaisedButton(
                        elevation: 10.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                            side: BorderSide(
                                color: Color.fromRGBO(16, 216, 200, 1.0),
                                width: 7.0)),
                        color: Color.fromRGBO(33, 41, 74, 1.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: size.height * 0.06),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(child: Container()),
                                Container(
                                    width: size.height * 0.09,
                                    height: size.height * 0.09,
                                    child: Image.asset(
                                        'assets/images/estrella.png')),
                                Expanded(child: Container()),
                                Container(
                                    width: size.height * 0.09,
                                    height: size.height * 0.09,
                                    child: Image.asset(
                                        'assets/images/estrella.png')),
                                Expanded(child: Container()),
                                Container(
                                    width: size.height * 0.09,
                                    height: size.height * 0.09,
                                    child: Image.asset(
                                        'assets/images/estrella.png')),
                                Expanded(child: Container()),
                              ],
                            ),
                            Expanded(child: Container()),
                            _botonAvanzado(),
                            SizedBox(height: size.height * 0.08),
                          ],
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, 'desafioescarpavanzado');
                        }),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                ],
              ),
            ),
            Expanded(child: Container()),
          ],
        ),
      ),
    );
  }

  Widget _textoBoton() {
    return Stack(
      alignment: Alignment(-0.25, -0.6),
      children: <Widget>[
        Text(
          'Desafio',
          style: TextStyle(
              fontSize: 15.0,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              fontFamily: 'LiquidBloss'),
        ),
        Text(
          'EscArp',
          style: TextStyle(
              fontSize: 40.0,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              fontFamily: 'Autography',
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }

  Widget _textoBotonIngles() {
    return Stack(
      alignment: Alignment(-0.25, -0.6),
      children: <Widget>[
        Text(
          'Challenge',
          style: TextStyle(
              fontSize: 10.0,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              fontFamily: 'GillSans'),
        ),
        Text(
          'EscArp',
          style: TextStyle(
              fontSize: 35.0,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              fontFamily: 'Autography',
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }

  Widget _textoTitulo() {
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return _textoBoton();
    } else {
      return _textoBotonIngles();
    }
  }

  Widget _botonFacil() {
    setState(() {});
    final size = MediaQuery.of(context).size;
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text('Facil',
          style: TextStyle(
              fontSize: (size.width * 0.2) * 0.2,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'LiquidBloss'));
    } else {
      return Text('Easy',
          style: TextStyle(
              fontSize: (size.width * 0.2) * 0.2,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'LiquidBloss'));
    }
  }

  Widget _botonMedio() {
    setState(() {});
    final size = MediaQuery.of(context).size;
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text('Media',
          style: TextStyle(
              fontSize: (size.width * 0.2) * 0.2,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'LiquidBloss'));
    } else {
      return Text('Medium',
          style: TextStyle(
              fontSize: (size.width * 0.2) * 0.2,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'LiquidBloss'));
    }
  }

  Widget _botonAvanzado() {
    setState(() {});
    final size = MediaQuery.of(context).size;
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text('Dificil',
          style: TextStyle(
              fontSize: (size.width * 0.2) * 0.2,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'LiquidBloss'));
    } else {
      return Text('Hard',
          style: TextStyle(
              // fontSize: size.width * 0.04,
              fontSize: (size.width * 0.2) * 0.2,
              color: Color.fromRGBO(243, 250, 175, 1.0),
              fontFamily: 'LiquidBloss'));
    }
  }
}
