import 'package:flutter/material.dart';

import 'package:package_info/package_info.dart';

class VersionPage extends StatefulWidget {
  @override
  _VersionPageState createState() => _VersionPageState();
}

class _VersionPageState extends State<VersionPage> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  Widget _infoTile(String title, String subtitle) {
    final size = MediaQuery.of(context).size;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
              fontSize: size.height * 0.04,
              color: Colors.white,
              fontFamily: 'GillSans'),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          width: size.width * 0.01,
        ),
        Text(
          subtitle ?? 'Not set',
          style: TextStyle(
              fontSize: size.height * 0.04,
              color: Colors.white,
              fontFamily: 'GillSans'),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Stack(children: <Widget>[
          Container(
            padding: EdgeInsets.all(5.0),
            alignment: Alignment.center,
            width: double.infinity,
            height: double.infinity,
            // color: Color.fromRGBO(33, 41, 74, 1.0),
            decoration: BoxDecoration(
                // borderRadius: BorderRadius.circular(80.0),
                gradient: RadialGradient(
              colors: [
                Color.fromRGBO(33, 41, 74, 1.0),
                Color.fromRGBO(33, 41, 74, 0.95),
              ],
              center: Alignment(0.0, 0.0),
              focal: Alignment(0.0, 0.0),
              focalRadius: 0.8,
            )),
            child: Column(
              children: <Widget>[
                SizedBox(height: size.height * 0.25),
                IconButton(
                  iconSize: size.height * 0.3,
                  onPressed: () {
                    // Navigator.pop(context);
                  },
                  icon: Image.asset(
                    'assets/images/icono_app.png',
                  ),
                ),

                // _infoTile('App name', _packageInfo.appName),
                // _infoTile('Package name', _packageInfo.packageName),
                _infoTile('Versión', _packageInfo.version),
                // _infoTile('Build number', _packageInfo.buildNumber),

                Expanded(child: Container()),
                IconButton(
                  iconSize: size.height * 0.26,
                  onPressed: () {
                    // Navigator.pop(context);
                  },
                  icon: Image.asset(
                    'assets/images/logo_mem.png',
                  ),
                ),
                // Expanded(child: Container()),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.transparent,
            child: FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: Colors.transparent,
                )),
          )
        ]),
      ),
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: Colors.transparent,
      //   onPressed: () {
      //     Navigator.pop(context);
      //   },
      //   child: Icon(
      //     Icons.keyboard_backspace_rounded,
      //     color: Color.fromRGBO(16, 216, 200, 1.0),
      //     size: size.height * 0.1,
      //   ),
      // )
    );
  }
}
