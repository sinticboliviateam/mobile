import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:escarp_app/share_prefs/preferencias_usuarios.dart';

class LibroPage extends StatefulWidget {
  @override
  _LibroPageState createState() => _LibroPageState();
}

class _LibroPageState extends State<LibroPage> {
  final prefs = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
      appBar: AppBar(
        leading: FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.keyboard_backspace_rounded,
            color: Color.fromRGBO(16, 216, 200, 1.0),
            size: size.height * 0.1,
          ),
        ),
        elevation: 0.0,
        iconTheme: IconThemeData(color: Color.fromRGBO(16, 216, 200, 1.0)),
        toolbarHeight: size.height * 0.2,
        backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
        centerTitle: true,
        title: _titulo(),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              color: Color.fromRGBO(243, 250, 175, 1.0),
            ),
            width: size.width * 0.9,
            height: 3,
          ),
          Expanded(child: Container()),
          Container(
            width: size.width * 0.9,
            child: _texto(),
          ),
          Expanded(child: Container()),
          IconButton(
            icon: Image.asset(
              'assets/icon/whatsapp_icon.png',
            ),
            color: Colors.white,
            iconSize: 60.0,
            onPressed: _launchURL,
          ),
          Expanded(child: Container()),
        ],
      )),
    );
  }

  Widget _texto() {
    final size = MediaQuery.of(context).size;
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text(
        'Contáctanos y adquiere tu libro.',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontFamily: 'GillSans',
          fontSize: size.height * 0.08,
        ),
      );
    } else {
      return Text(
        'Contact us and purchase your book.',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontFamily: 'GillSans',
          fontSize: size.height * 0.08,
        ),
      );
    }
  }

  Widget _titulo() {
    final size = MediaQuery.of(context).size;
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text('Libro EscArp',
          style: TextStyle(
            fontSize: size.height * 0.09,
            fontFamily: 'LiquidBloss',
            fontWeight: FontWeight.w900,
            color: Color.fromRGBO(16, 216, 200, 1.0),
          ));
    } else {
      return Text('EscArp Book',
          style: TextStyle(
            fontSize: size.height * 0.09,
            fontFamily: 'LiquidBloss',
            fontWeight: FontWeight.w900,
            color: Color.fromRGBO(16, 216, 200, 1.0),
          ));
    }
  }
}

_launchURL() async {
  const url =
      'https://wa.me/56957597741?text=Hola%20quiero%20reservar%20un%20libro';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
