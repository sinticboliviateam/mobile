import 'dart:ui';

import 'package:escarp_app/share_prefs/preferencias_usuarios.dart';
import 'package:flutter/material.dart';

import 'variacion_page.dart';

class VariacionesPage extends StatefulWidget {
  @override
  _VariacionesPageState createState() => _VariacionesPageState();
}

class _VariacionesPageState extends State<VariacionesPage> {
  final prefs = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        leading: FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.keyboard_backspace_rounded,
            color: Color.fromRGBO(16, 216, 200, 1.0),
            size: size.height * 0.1,
          ),
        ),
        elevation: 0.0,
        iconTheme: IconThemeData(color: Color.fromRGBO(16, 216, 200, 1.0)),
        // toolbarHeight: size.height * 0.2,
        backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
        centerTitle: true,
        title: _titulo(),
      ),
      backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded(child: Container()),
            Container(
              width: size.width * 0.65,
              height: size.height * 0.68,
              child: Stack(
                alignment: AlignmentDirectional.centerStart,
                children: <Widget>[_containerColumnas(), _filas()],
              ),
            ),
            Expanded(child: Container()),
          ],
        ),
        // SizedBox(height: size.height * 0.05),
        // ],
      ),
      // ),
    );
  }

  Widget _filas() {
    final size = MediaQuery.of(context).size;
    return Container(
        color: Colors.transparent,
        child: Column(children: <Widget>[
          // Expanded(
          //     child: Container(
          //         // color: Colors.red,
          //         )),

          SizedBox(
            height: size.height * 0.03,
          ),

          _containerfilasC(),

          Expanded(
              child: Container(
                  // color: Colors.red,
                  )),
          // SizedBox(
          //   height: size.height * 0.02,
          // ),
          _containerfilasB(),
          Expanded(
              child: Container(
                  // color: Colors.red,
                  )),
          // SizedBox(
          //   height: size.height * 0.02,
          // ),
          _containerfilasA(),
          SizedBox(
              // height: size.height * 0.01,
              ),
          _containerfilasNumeros(),
          SizedBox(
              // height: size.height * 0.01,
              ),

          _containerfilasFlechasEscArp(),

          // Expanded(child: Container()),
          // SizedBox(
          //   height: size.height * 0.03,
          // ),
        ]));
  }

  Widget _containerfilasC() {
    final size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(243, 250, 175, 0.16),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(children: <Widget>[
        Container(
          width: size.width * 0.02,
        ),
        _textoBotonC(),
        Expanded(child: Container()),
        _container2('C1'),
        Expanded(child: Container()),
        _container2('C2'),
        Expanded(child: Container()),
        _container2('C3'),
        Expanded(child: Container()),
        _container2('C4'),
        Container(
          width: size.width * 0.02,
        ),
      ]),
    );
  }

  Widget _containerfilasB() {
    final size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(243, 250, 175, 0.16),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(children: <Widget>[
        Container(
          width: size.width * 0.02,
        ),
        _textoBotonB(),
        Expanded(child: Container()),
        _container2('B1'),
        Expanded(child: Container()),
        _container2('B2'),
        Expanded(child: Container()),
        _container2('B3'),
        Expanded(child: Container()),
        _container2('B4'),
        Container(
          width: size.width * 0.02,
        ),
      ]),
    );
  }

  Widget _containerfilasA() {
    final size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(243, 250, 175, 0.16),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(children: <Widget>[
        Container(
          width: size.width * 0.02,
        ),
        _textoBotonA(),
        Expanded(child: Container()),
        _container2('A1'),
        Expanded(child: Container()),
        _container2('A2'),
        Expanded(child: Container()),
        _container2('A3'),
        Expanded(child: Container()),
        _container2('A4'),
        Container(
          width: size.width * 0.02,
        ),
      ]),
    );
  }

  Widget _containerfilasNumeros() {
    final size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        // color: Color.fromRGBO(243, 250, 175, 0.16),
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(children: <Widget>[
        Container(
          // color: Colors.red,
          width: size.width * 0.02,
        ),
        _container22(),
        Expanded(child: Container()),
        _container11('1'),
        Expanded(child: Container()),
        _container11('2'),
        Expanded(child: Container()),
        _container11('3'),
        Expanded(child: Container()),
        _container11('4'),
        Container(
          width: size.width * 0.02,
        ),
      ]),
    );
  }

  Widget _containerfilasFlechasEscArp() {
    final size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        // color: Color.fromRGBO(243, 250, 175, 0.16),
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(children: <Widget>[
        Container(
          // color: Colors.red,
          width: size.width * 0.02,
        ),
        _container22(),
        Expanded(child: Container()),
        _containerEscEsc(),
        Expanded(child: Container()),
        _containerArpArp(),
        Expanded(child: Container()),
        _containerEscArp(),
        Expanded(child: Container()),
        _containerArpEsc(),
        Container(
          width: size.width * 0.02,
        ),
      ]),
    );
  }

  Widget _container2(
    String texto,
  ) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.height / 9,
      height: size.height / 9,
      child: RaisedButton(
          elevation: 10.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
              side: BorderSide(
                color: Color.fromRGBO(16, 216, 200, 1.0),
                width: 3.0,
              )),
          padding: const EdgeInsets.all(0.0),
          color: Color.fromRGBO(33, 41, 74, 1.0),
          child: Text(texto,
              style: TextStyle(
                  fontSize: size.width * 0.03,
                  color: Color.fromRGBO(243, 250, 175, 1.0),
                  fontFamily: 'LiquidBloss')),
          onPressed: () {
            // Navigator.pushNamed(context, variaciones);

            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (ctxt) => VariacionPage(
                          nombre: "$texto - I",
                        )));
          }),
    );
  }

  Widget _container11(
    String texto,
  ) {
    final size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,
      width: size.height / 9,
      height: size.height / 9,
      child: Text(texto,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: size.height / 9 * 0.55,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              fontFamily: 'LiquidBloss')),
    );
  }

  Widget _container22() {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.height / 9,
      height: size.height / 9,
      color: Colors.transparent,
      // color: Color.fromRGBO(33, 41, 74, 1.0),

      // borderRadius: BorderRadius.circular(20.0),
    );
  }

  Widget _containerColumnas() {
    final size = MediaQuery.of(context).size;
    return Container(
      color: Colors.transparent,
      child: Row(children: <Widget>[
        Container(
          width: size.width * 0.02,
        ),
        _container22(),
        Expanded(child: Container()),
        _containerColumnasIndividuales(),
        Expanded(child: Container()),
        _containerColumnasIndividuales(),
        Expanded(child: Container()),
        _containerColumnasIndividuales(),
        Expanded(child: Container()),
        _containerColumnasIndividuales(),
        Container(
          width: size.width * 0.02,
        ),
      ]),
    );
  }

  Widget _containerColumnasIndividuales() {
    final size = MediaQuery.of(context).size;
    return Container(
        width: size.height / 9,
        height: double.infinity,
        // margin: EdgeInsets.all(0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Color.fromRGBO(255, 176, 176, 0.16),
        ));
  }

  Widget _containerEscEsc() {
    final size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,
      width: size.height / 9,
      height: size.height / 9,
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.arrow_upward,
                  size: size.height / 9 * 0.35,
                  color: Color.fromRGBO(255, 176, 176, 1.0)),
              Icon(Icons.arrow_downward,
                  size: size.height / 9 * 0.35,
                  color: Color.fromRGBO(255, 176, 176, 1.0)),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Esc',
                style: TextStyle(
                    fontSize: size.height / 9 * 0.23,
                    color: Color.fromRGBO(255, 176, 176, 1.0),
                    fontFamily: 'GillSans',
                    fontWeight: FontWeight.w100),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                'Esc',
                style: TextStyle(
                    fontSize: size.height / 9 * 0.23,
                    color: Color.fromRGBO(255, 176, 176, 1.0),
                    fontFamily: 'GillSans',
                    fontWeight: FontWeight.w100),
              ),
            ],
          ),
          // SizedBox(
          //   height: size.height * 0.03,
          // ),
        ],
      ),
    );
  }

  Widget _containerArpArp() {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.height / 9,
      height: size.height / 9,
      // margin: EdgeInsets.all(1.0),
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.arrow_upward,
                  size: size.height / 9 * 0.35,
                  color: Color.fromRGBO(243, 250, 175, 1.0)),
              Icon(Icons.arrow_downward,
                  size: size.height / 9 * 0.35,
                  color: Color.fromRGBO(243, 250, 175, 1.0)),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Arp',
                style: TextStyle(
                    fontSize: size.height / 9 * 0.21,
                    color: Color.fromRGBO(243, 250, 175, 1.0),
                    fontFamily: 'GillSans',
                    fontWeight: FontWeight.w100),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                'Arp',
                style: TextStyle(
                    fontSize: size.height / 9 * 0.21,
                    color: Color.fromRGBO(243, 250, 175, 1.0),
                    fontFamily: 'GillSans',
                    fontWeight: FontWeight.w100),
              ),
            ],
          ),
          // SizedBox(
          //   height: size.height * 0.03,
          // ),
        ],
      ),
    );
  }

  Widget _containerEscArp() {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.height / 9,
      height: size.height / 9,
      // margin: EdgeInsets.all(1.0),
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.arrow_upward,
                  size: size.height / 9 * 0.35,
                  color: Color.fromRGBO(255, 176, 176, 1.0)),
              Icon(Icons.arrow_downward,
                  size: size.height / 9 * 0.35,
                  color: Color.fromRGBO(243, 250, 175, 1.0)),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Esc',
                style: TextStyle(
                    fontSize: size.height / 9 * 0.23,
                    color: Color.fromRGBO(255, 176, 176, 1.0),
                    fontFamily: 'GillSans',
                    fontWeight: FontWeight.w100),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                'Arp',
                style: TextStyle(
                    fontSize: size.height / 9 * 0.21,
                    color: Color.fromRGBO(243, 250, 175, 1.0),
                    fontFamily: 'GillSans',
                    fontWeight: FontWeight.w100),
              ),
            ],
          ),
          // SizedBox(
          //   height: size.height * 0.03,
          // ),
        ],
      ),
    );
  }

  Widget _containerArpEsc() {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.height / 9,
      height: size.height / 9,
      alignment: Alignment.center,
      // margin: EdgeInsets.all(1.0),
      //
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.arrow_upward,
                  size: size.height / 9 * 0.35,
                  color: Color.fromRGBO(243, 250, 175, 1.0)),
              Icon(Icons.arrow_downward,
                  size: size.height / 9 * 0.35,
                  color: Color.fromRGBO(255, 176, 176, 1.0)),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Arp',
                style: TextStyle(
                    fontSize: size.height / 9 * 0.21,
                    color: Color.fromRGBO(243, 250, 175, 1.0),
                    fontFamily: 'GillSans',
                    fontWeight: FontWeight.w100),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                'Esc',
                style: TextStyle(
                    fontSize: size.height / 9 * 0.23,
                    color: Color.fromRGBO(255, 176, 176, 1.0),
                    fontFamily: 'GillSans',
                    fontWeight: FontWeight.w100),
              ),
            ],
          ),
          // SizedBox(
          //   height: size.height * 0.03,
          // ),
        ],
      ),
    );
  }

  Widget _textoBotonC() {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.height / 9,
      height: size.height / 9,
      child: Row(
        children: <Widget>[
          Container(
            child: Text(
              'C',
              style: TextStyle(
                  fontSize: size.width * 0.04,
                  color: Color.fromRGBO(16, 216, 200, 1.0),
                  fontFamily: 'LiquidBloss',
                  fontWeight: FontWeight.w100),
            ),
          ),
          Container(
            width: size.height / 15,
            height: size.height / 15,
            padding: EdgeInsets.all(0.0),
            alignment: Alignment.topCenter,
            child: Text(
              '(9º)',
              style: TextStyle(
                  fontSize: size.width * 0.017,
                  color: Color.fromRGBO(255, 176, 176, 1.0),
                  fontFamily: 'GillSans'),
            ),
          ),
        ],
      ),
    );
  }

  Widget _textoBotonB() {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.height / 9,
      height: size.height / 9,
      child: Row(
        children: <Widget>[
          Container(
            child: Text(
              'B',
              style: TextStyle(
                  fontSize: size.width * 0.04,
                  color: Color.fromRGBO(16, 216, 200, 1.0),
                  fontFamily: 'LiquidBloss',
                  fontWeight: FontWeight.w100),
            ),
          ),
          Container(
            width: size.height / 15,
            height: size.height / 15,
            // padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5),
            alignment: Alignment.topCenter,
            child: Text(
              '(7º)',
              style: TextStyle(
                  fontSize: size.width * 0.017,
                  color: Color.fromRGBO(255, 176, 176, 1.0),
                  fontFamily: 'GillSans'),
            ),
          ),
        ],
      ),
    );
  }

  Widget _textoBotonA() {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.height / 9,
      height: size.height / 9,
      child: Row(
        children: <Widget>[
          Container(
            child: Text(
              'A',
              style: TextStyle(
                  fontSize: size.width * 0.04,
                  color: Color.fromRGBO(16, 216, 200, 1.0),
                  fontFamily: 'LiquidBloss',
                  fontWeight: FontWeight.w100),
            ),
          ),
          Container(
            width: size.height / 15,
            height: size.height / 15,
            // padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5),
            alignment: Alignment.topCenter,
            child: Text(
              '(5º)',
              style: TextStyle(
                  fontSize: size.width * 0.017,
                  color: Color.fromRGBO(255, 176, 176, 1.0),
                  fontFamily: 'GillSans'),
            ),
          ),
        ],
      ),
    );
  }

  Widget _titulo() {
    final size = MediaQuery.of(context).size;
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text('Variaciones',
          style: TextStyle(
            fontSize: size.height * 0.09,
            fontFamily: 'LiquidBloss',
            fontWeight: FontWeight.w900,
            color: Color.fromRGBO(16, 216, 200, 1.0),
          ));
    } else {
      return Text('Variations',
          style: TextStyle(
            fontSize: size.height * 0.09,
            fontFamily: 'LiquidBloss',
            fontWeight: FontWeight.w900,
            color: Color.fromRGBO(16, 216, 200, 1.0),
          ));
    }
  }
}
