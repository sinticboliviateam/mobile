import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:escarp_app/share_prefs/preferencias_usuarios.dart';
import 'package:flutter/material.dart';
import 'package:roller_list/roller_list.dart';

// import 'variacion_page.dart';

class SlotMachine extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SlotMachineState();
  }
}

class _SlotMachineState extends State<SlotMachine> {
  AudioCache audioCache = AudioCache();
  AudioPlayer advancedPlayer = AudioPlayer();
  final prefs = new PreferenciasUsuario();

  @override
  void initState() {
    first = 0;
    second = 0;
    third = 0;
    super.initState();

    if (Platform.isIOS) {
      if (audioCache.fixedPlayer != null) {
        audioCache.fixedPlayer.startHeadlessService();
      }
    }
  }

  static const _ROTATION_DURATION = Duration(milliseconds: 500);
  final List<Widget> slots = _getSlots();
  final List<Widget> slots1 = _getSlots1();
  final List<Widget> slots2 = _getSlots2();
  bool statusBotonAyuda = false;
  final List<String> slotNames = [
    "A1",
    "A2",
    "A3",
    "A4",
    "B1",
    "B2",
    "B3",
    "B4",
    "C1",
    "C2",
    "C3",
    "C4"
  ];
  final List<String> slotNames1 = [
    "I",
    "II",
    "III",
    "I",
    "II",
    "III",
    "I",
    "II",
    "III",
    "I",
    "II",
    "III"
  ];
  final List<String> slotNames2 = [
    "DO",
    "RE",
    "MI",
    "FA",
    "SOL",
    "LA",
    "SI",
    "SOLb",
    "REb",
    "LAb",
    "MIb",
    "SIb"
  ];
  int first, second, third;
  final leftRoller = new GlobalKey<RollerListState>();
  final rightRoller = new GlobalKey<RollerListState>();
  Timer rotator;
  Random _random = new Random();

  @override
  void dispose() {
    rotator?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
      appBar: AppBar(
        leading: FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.keyboard_backspace_rounded,
            color: Color.fromRGBO(16, 216, 200, 1.0),
            size: size.height * 0.1,
          ),
        ),
        elevation: 0.0,
        iconTheme: IconThemeData(color: Color.fromRGBO(16, 216, 200, 1.0)),
        toolbarHeight: size.height * 0.2,
        backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
        centerTitle: true,
        title: _textoTitulo(),
        // actions: <Widget>[_botonAyuda()],
      ),
      body: Column(
        children: <Widget>[
          // Container(
          //   decoration: BoxDecoration(
          //     borderRadius: BorderRadius.circular(5.0),
          //     color: Color.fromRGBO(243, 250, 175, 1.0),
          //   ),
          //   width: size.width * 0.9,
          //   height: 3,
          // ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  color: Color.fromRGBO(243, 250, 175, 1.0),
                ),
                width: size.width * 0.6,
                height: size.height * 0.65,
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      height:
                          (size.height * 0.65 - (size.height * 0.65 - 57)) / 2,
                      child: _textoParaGirar(),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                        borderRadius: BorderRadius.circular(10.0),
                        color: Color.fromRGBO(243, 250, 175, 1.0),
                      ),
                      width: size.width * 0.6 - 57,
                      height: size.height * 0.65 - 57,
                      alignment: Alignment.center,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0),
                          color: Color.fromRGBO(16, 196, 150, 1.0),
                        ),
                        width: size.width * 0.6 - 80,
                        height: size.height * 0.65 - 80,
                        alignment: Alignment.center,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Color.fromRGBO(16, 216, 200, 1.0),
                          ),
                          width: size.width * 0.6 - 120,
                          height: size.height * 0.65 - 100,
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7.0),
                                    gradient: LinearGradient(
                                      colors: [
                                        Color.fromRGBO(155, 155, 155, 0.8),
                                        Color.fromRGBO(155, 155, 155, 0.6),
                                        Color.fromRGBO(155, 155, 155, 0.5),
                                        Color.fromRGBO(155, 155, 155, 0.3),
                                        Color.fromRGBO(155, 155, 155, 0.5),
                                        Color.fromRGBO(155, 155, 155, 0.6),
                                        Color.fromRGBO(155, 155, 155, 0.8),
                                      ],
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                    )),
                                alignment: Alignment.center,
                                width: size.width * 0.11,
                                height: size.height * 0.28,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5.0),
                                      gradient: LinearGradient(
                                        colors: [
                                          Color.fromRGBO(0, 0, 0, 0.6),
                                          Color.fromRGBO(0, 0, 0, 0.5),
                                          Color.fromRGBO(0, 0, 0, 0.4),
                                          Color.fromRGBO(0, 0, 0, 0.3),
                                          Color.fromRGBO(0, 0, 0, 0.4),
                                          Color.fromRGBO(0, 0, 0, 0.5),
                                          Color.fromRGBO(0, 0, 0, 0.6),
                                        ],
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                      )),
                                  alignment: Alignment.center,
                                  width: size.width * 0.1,
                                  height: size.height * 0.23,
                                  child: Container(
                                    width: size.width * 0.1,
                                    height: size.height * 0.2,
                                    color: Colors.transparent,
                                    child: RollerList(
                                      items: slots,
                                      enabled: false,
                                      key: leftRoller,
                                      onSelectedIndexChanged: (value) {
                                        setState(() {
                                          if (value > 0) {
                                            first = value - 1;
                                          } else if (value == 0) {
                                            first = 11;
                                          }
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              VerticalDivider(
                                width: 10,
                                color: Colors.transparent,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7.0),
                                    gradient: LinearGradient(
                                      colors: [
                                        Color.fromRGBO(155, 155, 155, 0.8),
                                        Color.fromRGBO(155, 155, 155, 0.6),
                                        Color.fromRGBO(155, 155, 155, 0.5),
                                        Color.fromRGBO(155, 155, 155, 0.3),
                                        Color.fromRGBO(155, 155, 155, 0.5),
                                        Color.fromRGBO(155, 155, 155, 0.6),
                                        Color.fromRGBO(155, 155, 155, 0.8),
                                      ],
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                    )),
                                alignment: Alignment.center,
                                width: size.width * 0.11,
                                height: size.height * 0.28,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5.0),
                                      gradient: LinearGradient(
                                        colors: [
                                          Color.fromRGBO(0, 0, 0, 0.6),
                                          Color.fromRGBO(0, 0, 0, 0.5),
                                          Color.fromRGBO(0, 0, 0, 0.4),
                                          Color.fromRGBO(0, 0, 0, 0.3),
                                          Color.fromRGBO(0, 0, 0, 0.4),
                                          Color.fromRGBO(0, 0, 0, 0.5),
                                          Color.fromRGBO(0, 0, 0, 0.6),
                                        ],
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                      )),
                                  alignment: Alignment.center,
                                  width: size.width * 0.1,
                                  height: size.height * 0.23,
                                  child: Container(
                                    width: size.width * 0.1,
                                    height: size.height * 0.2,
                                    color: Colors.transparent,
                                    alignment: Alignment.center,
                                    child: RollerList(
                                      items: slots1,
                                      scrollType: ScrollType.bothDirections,
                                      onSelectedIndexChanged: (value) {
                                        setState(() {
                                          if (value > 0) {
                                            second = value - 1;
                                          } else if (value == 0) {
                                            second = 11;
                                          }
                                        });
                                        _finishRotating();
                                      },
                                      onScrollStarted: _startRotating,
                                    ),
                                  ),
                                ),
                              ),
                              VerticalDivider(
                                width: 10,
                                color: Colors.transparent,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7.0),
                                    gradient: LinearGradient(
                                      colors: [
                                        Color.fromRGBO(155, 155, 155, 0.8),
                                        Color.fromRGBO(155, 155, 155, 0.6),
                                        Color.fromRGBO(155, 155, 155, 0.5),
                                        Color.fromRGBO(155, 155, 155, 0.3),
                                        Color.fromRGBO(155, 155, 155, 0.5),
                                        Color.fromRGBO(155, 155, 155, 0.6),
                                        Color.fromRGBO(155, 155, 155, 0.8),
                                      ],
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                    )),
                                alignment: Alignment.center,
                                width: size.width * 0.11,
                                height: size.height * 0.28,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5.0),
                                      gradient: LinearGradient(
                                        colors: [
                                          Color.fromRGBO(0, 0, 0, 0.6),
                                          Color.fromRGBO(0, 0, 0, 0.5),
                                          Color.fromRGBO(0, 0, 0, 0.4),
                                          Color.fromRGBO(0, 0, 0, 0.3),
                                          Color.fromRGBO(0, 0, 0, 0.4),
                                          Color.fromRGBO(0, 0, 0, 0.5),
                                          Color.fromRGBO(0, 0, 0, 0.6),
                                        ],
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                      )),
                                  alignment: Alignment.center,
                                  width: size.width * 0.1,
                                  height: size.height * 0.23,
                                  child: Container(
                                    width: size.width * 0.1,
                                    height: size.height * 0.2,
                                    color: Colors.transparent,
                                    alignment: Alignment.center,
                                    child: RollerList(
                                      enabled: false,
                                      items: slots2,
                                      key: rightRoller,
                                      onSelectedIndexChanged: (value) {
                                        setState(() {
                                          if (value > 0) {
                                            third = value - 1;
                                          } else if (value == 0) {
                                            third = 11;
                                          }
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height:
                          (size.height * 0.65 - (size.height * 0.65 - 57)) / 2,
                    )
                  ],
                ),
              ),
            ),
          ),
          // Expanded(
          //     child: Container(
          //   child: Column(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: <Widget>[
          //       Text('Resultado:',
          //           style: TextStyle(
          //               fontSize: 25.0,
          //               color: Color.fromRGBO(243, 250, 175, 1.0),
          //               fontFamily: 'LiquidBloss')),
          //       SizedBox(height: 15.0),
          //       Text(
          //           "${slotNames[first]}  ${slotNames1[second]} ${slotNames2[third]}",
          //           style: TextStyle(
          //               fontSize: 35.0,
          //               color: Color.fromRGBO(243, 250, 175, 1.0),
          //               fontFamily: 'LiquidBloss')),
          //       SizedBox(height: 15.0),
          //       Container(
          //         width: size.width * 0.25,
          //         height: size.height * 0.15,
          //         child: RaisedButton(
          //             color: Color.fromRGBO(16, 216, 200, 0.5),
          //             child: Text('Ir a la ayuda',
          //                 style: TextStyle(
          //                     fontSize: 25.0,
          //                     color: Color.fromRGBO(243, 250, 175, 1.0),
          //                     fontFamily: 'LiquidBloss')),
          //             onPressed: () {
          //               Navigator.pushNamed(context,
          //                   'variacion${slotNames[first]}_${slotNames1[second]}');
          //             }),
          //       )
          //     ],
          //   ),
          // ))
          //   ],
          // ),
        ],
      ),
    );
  }

  void _startRotating() {
    statusBotonAyuda = false;
    audioCache.play('audios/ruleta.mp3');
    rotator = Timer.periodic(_ROTATION_DURATION, _rotateRoller);
  }

  void _rotateRoller(_) {
    final leftRotationTarget = _random.nextInt(3 * slots1.length);
    final rightRotationTarget = _random.nextInt(3 * slots2.length);
    leftRoller.currentState?.smoothScrollToIndex(leftRotationTarget,
        duration: _ROTATION_DURATION, curve: Curves.linear);
    rightRoller.currentState?.smoothScrollToIndex(rightRotationTarget,
        duration: _ROTATION_DURATION, curve: Curves.linear);
  }

  void _finishRotating() {
    statusBotonAyuda = true;
    rotator?.cancel();
  }

  static List<Widget> _getSlots() {
    List<Widget> result = new List();
    for (int i = 0; i <= 11; i++) {
      result.add(Container(
        // padding: EdgeInsets.all(4.0),
        color: Colors.transparent,
        child: Image.asset(
          "assets/variaciones/$i.jpg",
          width: double.infinity,
          height: double.infinity,
        ),
      ));
    }
    return result;
  }

  static List<Widget> _getSlots1() {
    List<Widget> result = new List();
    for (int i = 0; i <= 11; i++) {
      result.add(Container(
        // padding: EdgeInsets.all(4.0),
        color: Colors.transparent,
        child: Image.asset(
          "assets/articulaciones/$i.jpg",
          width: double.infinity,
          height: double.infinity,
        ),
      ));
    }
    return result;
  }

  static List<Widget> _getSlots2() {
    final prefs = new PreferenciasUsuario();

    List<Widget> result = new List();
    if (prefs.idioma == true) {
      for (int i = 0; i <= 11; i++) {
        result.add(Container(
          // padding: EdgeInsets.all(4.0),
          color: Colors.transparent,
          child: Image.asset(
            "assets/tonos/$i.jpg",
            width: double.infinity,
            height: double.infinity,
          ),
        ));
      }
      return result;
    } else {
      for (int i = 0; i <= 11; i++) {
        result.add(Container(
          // padding: EdgeInsets.all(4.0),
          color: Colors.transparent,
          child: Image.asset(
            "assets/tonos_ingles/$i.png",
            width: double.infinity,
            height: double.infinity,
          ),
        ));
      }
      return result;
    }
  }

  Widget _textoBoton() {
    return Stack(
      alignment: Alignment(-0.25, -0.6),
      children: <Widget>[
        Text(
          'Desafio',
          style: TextStyle(
              fontSize: 15.0,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              fontFamily: 'LiquidBloss'),
        ),
        Text(
          'EscArp',
          style: TextStyle(
              fontSize: 40.0,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              fontFamily: 'Autography',
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }

  // Widget _botonAyuda() {
  //   final size = MediaQuery.of(context).size;
  //   if (statusBotonAyuda == false) {
  //     return Row(
  //       children: [
  //         Container(
  //             child: Icon(
  //           Icons.help_outline_rounded,
  //           color: Color.fromRGBO(16, 216, 200, 0.5),
  //         )),
  //         Container(
  //           width: 30.0,
  //         )
  //       ],
  //     );
  //   } else {
  //     return FlatButton(
  //         color: Color.fromRGBO(33, 41, 74, 1.0),
  //         child: Icon(
  //           Icons.help_outline_rounded,
  //           color: Color.fromRGBO(16, 216, 200, 1.0),
  //           size: size.height * 0.1,
  //         ),
  //         onPressed: () {
  //           // Navigator.pushNamed(
  //           //     context, 'variacion${slotNames[first]}_${slotNames1[second]}');

  //           Navigator.push(
  //               context,
  //               MaterialPageRoute(
  //                   builder: (ctxt) => VariacionPage(
  //                         nombre: '${slotNames[first]} - ${slotNames1[second]}',
  //                       )));
  //           statusBotonAyuda = false;
  //           setState(() {});
  //         });
  //   }
  // }

  Widget _textoBotonIngles() {
    return Stack(
      alignment: Alignment(-0.25, -0.6),
      children: <Widget>[
        Text(
          'Challenge',
          style: TextStyle(
              fontSize: 10.0,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              fontFamily: 'GillSans'),
        ),
        Text(
          'EscArp',
          style: TextStyle(
              fontSize: 35.0,
              color: Color.fromRGBO(16, 216, 200, 1.0),
              fontFamily: 'Autography',
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }

  Widget _textoTitulo() {
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return _textoBoton();
    } else {
      return _textoBotonIngles();
    }
  }

  Widget _textoParaGirar() {
    final size = MediaQuery.of(context).size;
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text(
        'Arrastra el dedo para girar la tómbola',
        style: TextStyle(
            color: Color.fromRGBO(33, 41, 74, 1.0),
            fontFamily: 'GillSanz',
            fontSize: size.height * 0.03),
        textAlign: TextAlign.center,
      );
    } else {
      return Text('Drag your finger to spin the slotmachine',
          style: TextStyle(
              color: Color.fromRGBO(33, 41, 74, 1.0),
              fontFamily: 'GillSanz',
              fontSize: size.height * 0.03),
          textAlign: TextAlign.center);
    }
  }
}
