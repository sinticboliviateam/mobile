import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:escarp_app/share_prefs/preferencias_usuarios.dart';

class BienvenidaPage extends StatefulWidget {
  @override
  _BienvenidaPageState createState() => _BienvenidaPageState();
}

class _BienvenidaPageState extends State<BienvenidaPage> {
  final prefs = new PreferenciasUsuario();
  @override
  void initState() {
    //SPLASH SCREEN
    super.initState();

    Future.delayed(Duration(seconds: 5), () {
      Navigator.pushReplacementNamed(context, 'home');
    });
  }

  @override
  Widget build(BuildContext context) {
    // Set landscape orientation
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    // final size = MediaQuery.of(context).size;
    return Scaffold(
      // backgroundColor:Color.fromRGBO(33, 41, 74, 1.0),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(5.0),
          alignment: Alignment.center,
          width: double.infinity,
          height: double.infinity,
          // color: Color.fromRGBO(33, 41, 74, 1.0),
          decoration: BoxDecoration(
              // borderRadius: BorderRadius.circular(80.0),
              gradient: RadialGradient(
            colors: [
              Color.fromRGBO(33, 41, 74, 1.0),
              Color.fromRGBO(33, 41, 74, 0.95),
            ],
            center: Alignment(0.0, 0.0),
            focal: Alignment(0.0, 0.0),
            focalRadius: 0.8,
          )),
          child: Column(
            children: <Widget>[
              Expanded(child: Container()),
              // SizedBox(height: size.height * 0.13),
              Text(
                'EscArp',
                style: TextStyle(
                  fontFamily: 'Autography',
                  fontSize: 100,
                  color: Color.fromRGBO(16, 216, 200, 1.0),
                  fontWeight: FontWeight.w900,
                ),
                textAlign: TextAlign.center,
              ),
              Expanded(child: Container()),
              // SizedBox(height: size.height * 0.03),
              _texto(),
              Expanded(child: Container()),
              // SizedBox(height: size.height * 0.02),
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromRGBO(245, 250, 177, 1.0),
                ),
                child: Text(
                  'EA',
                  style: TextStyle(
                      fontFamily: 'Autography',
                      fontSize: 20.0,
                      color: Color.fromRGBO(33, 41, 74, 1.0),
                      fontWeight: FontWeight.w900),
                  textAlign: TextAlign.center,
                ),
              ),
              Expanded(child: Container()),
              // SizedBox(height: size.height * 0.06),
            ],
          ),
        ),
      ),
    );
  }

  Widget _texto() {
    final prefs = new PreferenciasUsuario();
    if (prefs.idioma == true) {
      return Text(
        '- Guía Practica de Escalas y Arpegios -',
        style: TextStyle(
          fontFamily: 'GillSans',
          fontSize: 20.0,
          color: Colors.white,
        ),
        textAlign: TextAlign.center,
      );
    } else {
      return Text(
        '- Scales and Arpeggios Practical Guide -',
        style: TextStyle(
          fontFamily: 'GillSans',
          fontSize: 20.0,
          color: Colors.white,
        ),
        textAlign: TextAlign.center,
      );
    }
  }

  // Widget _colorFondo1(){
  //   final size= MediaQuery.of(context).size;

  //   return Container(
  //      padding: EdgeInsets.all(5.0),
  //         alignment: Alignment.center,
  //         width: double.infinity,
  //         height: double.infinity,

  //        decoration: new BoxDecoration(
  //               color: Colors.white,
  //               border: Border.all(color: Color.fromRGBO(33, 41, 74, 1.0), width: 0.0),
  //               borderRadius: new BorderRadius.all(Radius.elliptical(size.height *0.9, size.height *0.5)),

  //           )
  //   );

  // }
  //  Widget _colorFondo2(){
  //   final size= MediaQuery.of(context).size;

  //   return Container(
  //      padding: EdgeInsets.all(5.0),
  //         alignment: Alignment.center,
  //         width: double.infinity,
  //         height: double.infinity,

  //        decoration: new BoxDecoration(
  //               // color: Colors.green,
  //               // border: Border.all(color: Colors.black, width: 0.0),
  //               borderRadius: new BorderRadius.all(Radius.elliptical(size.height *0.9, size.height *0.5)),
  //               gradient:  RadialGradient(
  //               colors: [
  //                 Color.fromRGBO(33, 41, 74, 1.0),
  //                 Color.fromRGBO(33, 41, 74, 0.9),
  //               ],
  //               center: Alignment(0.0, 0.0),
  //                 focal: Alignment(0.0, 0.0),
  //                 focalRadius:0.5,

  //             ),

  //           )
  //   );

  // }
}
