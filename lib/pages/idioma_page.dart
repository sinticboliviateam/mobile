import 'package:escarp_app/share_prefs/preferencias_usuarios.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

class IdiomaPage extends StatefulWidget {
  @override
  _IdiomaPageState createState() => _IdiomaPageState();
}

class _IdiomaPageState extends State<IdiomaPage> {
  final prefs = new PreferenciasUsuario();
  bool _idioma;

  @override
  void initState() {
    super.initState();

    setState(() {
      _idioma = prefs.idioma;
      print('IDIOMA:   ${prefs.idioma}');
    });
  }

  _setSelectIdioma(bool valor) {
    setState(() {
      prefs.idioma = valor;
      Phoenix.rebirth(context);

      _idioma = valor;
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
        appBar: AppBar(
            leading: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.keyboard_backspace_rounded,
                color: Color.fromRGBO(16, 216, 200, 1.0),
                size: size.height * 0.1,
              ),
            ),
            elevation: 0.0,
            iconTheme: IconThemeData(color: Color.fromRGBO(16, 216, 200, 1.0)),
            toolbarHeight: size.height * 0.2,
            backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
            centerTitle: true,
            title: _lenguajeTitulo()),
        body: Center(
            child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                color: Color.fromRGBO(243, 250, 175, 1.0),
              ),
              width: size.width * 0.9,
              height: 3,
            ),
            Expanded(child: Container()),
            SwitchListTile(
              activeColor: Color.fromRGBO(16, 216, 200, 1.0),
              inactiveThumbColor: Colors.red,
              inactiveTrackColor: Colors.red[300],
              value: _idioma,
              title: _lenguaje(),
              onChanged: _setSelectIdioma,
            ),
            Expanded(child: Container()),
            // Container(
            //     width: 150.0,
            //     height: 150.0,
            //     // child: Image.asset('assets/images/chile_flag.png')
            //     child: _bandera()),
            Expanded(child: Container()),
          ],
        )));
  }

  // Widget _bandera() {
  //   if (_idioma == true) {
  //     return Image.asset('assets/images/chile_flag.png');
  //   } else {
  //     return Image.asset('assets/images/inglaterra_flag.png');
  //   }
  // }

  Widget _lenguaje() {
    if (_idioma == true) {
      return Container(
        padding: EdgeInsets.all(20.0),
        child: Text('Idioma: Español',
            style: TextStyle(
                fontSize: 25.0,
                color: Color.fromRGBO(243, 250, 175, 1.0),
                fontFamily: 'GillSans')),
      );
    } else {
      return Container(
        padding: EdgeInsets.all(20.0),
        child: Text('Language: English',
            style: TextStyle(
                fontSize: 25.0,
                color: Color.fromRGBO(243, 250, 175, 1.0),
                fontFamily: 'GillSans')),
      );
    }
  }

  Widget _lenguajeTitulo() {
    final size = MediaQuery.of(context).size;
    if (_idioma == true) {
      return Text('Idioma',
          style: TextStyle(
            fontSize: size.height * 0.09,
            fontFamily: 'LiquidBloss',
            fontWeight: FontWeight.w900,
            color: Color.fromRGBO(16, 216, 200, 1.0),
          ));
    } else {
      return Text('Language',
          style: TextStyle(
            fontSize: size.height * 0.09,
            fontFamily: 'LiquidBloss',
            fontWeight: FontWeight.w900,
            color: Color.fromRGBO(16, 216, 200, 1.0),
          ));
    }
  }
}
