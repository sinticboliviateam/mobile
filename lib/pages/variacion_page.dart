import 'package:flutter/material.dart';
import 'package:native_video_view/native_video_view.dart';

class Media {
  final String path;
  final String texto;
  Media({this.path, this.texto});
}

class VariacionPage extends StatefulWidget {
  final String nombre;

  VariacionPage({this.nombre});

  @override
  _VariacionPageState createState() => _VariacionPageState();
}

class _VariacionPageState extends State<VariacionPage> {
  VideoViewController _controller;
  bool _isPlaying = false;

  Media _media;

  @override
  void initState() {
    _buscarVideo(widget.nombre);

    super.initState();
  }

  _buscarVideo(String nombre) {
    switch (nombre) {

      //////////////               A1            ///////////////////////////////
      case "A1 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/A1_I.mp4',
              texto:
                  "sube con escala hasta la 5ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "A1 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/A1_II.mp4',
              texto:
                  "sube con escala hasta la 5ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "A1 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/A1_III.mp4',
              texto:
                  "sube con escala hasta la 5ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "A2 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/A2_I.mp4',
              texto:
                  "Sube con arpegio hasta la 5ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "A2 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/A2_II.mp4',
              texto:
                  "Sube con arpegio hasta la 5ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "A2 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/A2_III.mp4',
              texto:
                  "Sube con arpegio hasta la 5ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "A3 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/A3_I.mp4',
              texto:
                  "Sube con escala hasta la 5ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "A3 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/A3_II.mp4',
              texto:
                  "Sube con escala hasta la 5ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "A3 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/A3_III.mp4',
              texto:
                  "Sube con escala hasta la 5ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "A4 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/A4_I.mp4',
              texto:
                  "Sube con arpegio hasta la 5ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "A4 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/A4_II.mp4',
              texto:
                  "Sube con arpegio hasta la 5ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "A4 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/A4_III.mp4',
              texto:
                  "Sube con arpegio hasta la 5ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;

      //////////////               B1            ///////////////////////////////
      case "B1 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/B1_I.mp4',
              texto:
                  "Sube con escala hasta la 7ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "B1 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/B1_II.mp4',
              texto:
                  "Sube con escala hasta la 7ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "B1 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/B1_III.mp4',
              texto:
                  "Sube con escala hasta la 7ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "B2 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/B2_I.mp4',
              texto:
                  "Sube con arpegio hasta la 7ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "B2 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/B2_II.mp4',
              texto:
                  "Sube con arpegio hasta la 7ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "B2 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/B2_III.mp4',
              texto:
                  "Sube con arpegio hasta la 7ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "B3 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/B3_I.mp4',
              texto:
                  "Sube con escala hasta la 7ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "B3 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/B3_II.mp4',
              texto:
                  "Sube con escala hasta la 7ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "B3 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/B3_III.mp4',
              texto:
                  "Sube con escala hasta la 7ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "B4 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/B4_I.mp4',
              texto:
                  "Sube con arpegio hasta la 7ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "B4 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/B4_II.mp4',
              texto:
                  "Sube con arpegio hasta la 7ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "B4 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/B4_III.mp4',
              texto:
                  "Sube con arpegio hasta la 7ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;

      //////////////               C1            ///////////////////////////////
      case "C1 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/C1_I.mp4',
              texto:
                  "Sube con escala hasta la 9ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "C1 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/C1_II.mp4',
              texto:
                  "Sube con escala hasta la 9ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "C1 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/C1_III.mp4',
              texto:
                  "Sube con escala hasta la 9ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "C2 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/C2_I.mp4',
              texto:
                  "Sube con arpegio hasta la 9ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "C2 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/C2_II.mp4',
              texto:
                  "Sube con arpegio hasta la 9ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "C2 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/C2_III.mp4',
              texto:
                  "Sube con arpegio hasta la 9ª nota y baja con escala 2 o más veces consecutivas.");
        });
        break;
      case "C3 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/C3_I.mp4',
              texto:
                  "Sube con escala hasta la 9ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "C3 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/C3_II.mp4',
              texto:
                  "Sube con escala hasta la 9ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "C3 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/C3_III.mp4',
              texto:
                  "Sube con escala hasta la 9ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
        break;
      case "C4 - I":
        setState(() {
          _media = Media(
              path: 'assets/videos/C4_I.mp4',
              texto:
                  "Sube con arpegio hasta la 9ª nota y baja con escala  2 o más veces consecutivas.");
        });
        break;
      case "C4 - II":
        setState(() {
          _media = Media(
              path: 'assets/videos/C4_II.mp4',
              texto:
                  "Sube con arpegio hasta la 9ª nota y baja con escala  2 o más veces consecutivas.");
        });
        break;
      case "C4 - III":
        setState(() {
          _media = Media(
              path: 'assets/videos/C4_III.mp4',
              texto:
                  "Sube con arpegio hasta la 9ª nota y baja con escala  2 o más veces consecutivas.");
        });
        break;
      default:
        setState(() {
          _media = Media(
              path: "'assets/videos/A1_I.mp4'",
              texto:
                  "Sube con arpegio hasta la 7ª nota y baja con arpegio 2 o más veces consecutivas.");
        });
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        leading: FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.keyboard_backspace_rounded,
            color: Color.fromRGBO(16, 216, 200, 1.0),
            size: size.height * 0.1,
          ),
        ),
        elevation: 0.0,
        iconTheme: IconThemeData(color: Color.fromRGBO(16, 216, 200, 1.0)),
        toolbarHeight: size.height * 0.2,
        backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
        centerTitle: true,
        title: Text(
          widget.nombre.substring(0, 2),
          style: TextStyle(
            fontSize: size.height * 0.10,
            fontFamily: 'LiquidBloss',
            fontWeight: FontWeight.w900,
            color: Color.fromRGBO(16, 216, 200, 1.0),
          ),
        ),
      ),
      backgroundColor: Color.fromRGBO(33, 41, 74, 1.0),
      body: Column(
        children: <Widget>[
          // Container(
          //   decoration: BoxDecoration(
          //     borderRadius: BorderRadius.circular(5.0),
          //     color: Color.fromRGBO(243, 250, 175, 1.0),
          //   ),
          //   width: size.width * 0.9,
          //   height: 3,
          // ),
          Expanded(
            child: Container(),
          ),
          // SizedBox(
          //   height: 20,
          // ),
          // Container(
          //   width: size.width * 0.8,
          //   child: Text(
          //     '${widget.nombre}: ${_media.texto}',
          //     style: TextStyle(
          //         fontSize: 20.0, color: Colors.white, fontFamily: 'GillSanz'),
          //     textAlign: TextAlign.center,
          //   ),
          // ),
          //  SizedBox(
          //   height: 20,
          // ),
          Container(
            padding: EdgeInsets.all(20.0),
            width: size.width * 0.98,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              color: Color.fromRGBO(33, 41, 74, 1.0),
              border: Border.all(
                color: Color.fromRGBO(33, 41, 74, 1.0),
                width: 5,
              ),
            ),
            child: Center(
                child: AspectRatio(
              aspectRatio: 5.0,
              child: NativeVideoView(
                keepAspectRatio: false,
                showMediaController: false,
                enableVolumeControl: true,
                onCreated: (controller) {
                  controller.setVideoSource(
                    _media.path,
                    sourceType: VideoSourceType.asset,
                    requestAudioFocus: true,
                  );

                  setState(() {
                    _controller = controller;
                  });
                },
                onPrepared: (controller, info) {
                  controller.play();
                  controller.pause();
                },
                onError: (controller, what, extra, message) {
                  print('Player Error ($what | $extra | $message)');
                },
                onCompletion: (controller) {
                  print('Video completed');
                },
              ),
            )),
          ),

          Expanded(
            child: Container(),
          ),
        ],
      ),
      floatingActionButton: _controller != null
          ? Container(
              width: size.height * 0.1,
              height: size.height * 0.1,
              child: FloatingActionButton(
                backgroundColor: Color.fromRGBO(16, 216, 200, 1.0),
                onPressed: () {
                  setState(() async {
                    final bool isPlaying = await _controller.isPlaying();
                    if (isPlaying) {
                      _controller.pause();
                      setState(() {
                        _isPlaying = false;
                      });
                    } else {
                      _controller.play();
                      setState(() {
                        _isPlaying = true;
                      });
                    }
                  });
                },
                child: Icon(
                  _isPlaying ? Icons.pause : Icons.play_arrow,
                  color: Color.fromRGBO(33, 41, 74, 1.0),
                ),
              ),
            )
          : Container(),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
