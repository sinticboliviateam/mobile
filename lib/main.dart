import 'package:escarp_app/pages/bienvenida_page.dart';
import 'package:escarp_app/pages/desafio_escarp_page.dart';
import 'package:escarp_app/pages/libro_escarp_page.dart';
import 'package:escarp_app/pages/home_page.dart';
import 'package:escarp_app/pages/idioma_page.dart';
import 'package:escarp_app/pages/tragaperras_avanzado_page.dart';
import 'package:escarp_app/pages/tragaperras_basico_page.dart';
import 'package:escarp_app/pages/tragaperras_intermedio_page.dart';
import 'package:escarp_app/pages/variaciones_page.dart';
import 'package:escarp_app/pages/version_page.dart';
import 'package:escarp_app/pages/tutorial_page.dart';
import 'package:escarp_app/share_prefs/preferencias_usuarios.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_phoenix/flutter_phoenix.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();

  await prefs.initPrefs();

  runApp(Phoenix(child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light
        .copyWith(statusBarColor: Colors.transparent));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'EscArp',
      theme: ThemeData(fontFamily: 'Sindentosa'),
      initialRoute: 'bienvenida',
      routes: {
        'bienvenida': (BuildContext context) => BienvenidaPage(),
        'home': (BuildContext context) => HomePage(),
        'version': (BuildContext context) => VersionPage(),
        'tutorial': (BuildContext context) => TutorialPage(),
        'variaciones': (BuildContext context) => VariacionesPage(),
        'idioma': (BuildContext context) => IdiomaPage(),
        'libro': (BuildContext context) => LibroPage(),
        'desafioescarp': (BuildContext context) => DesafioEscArp(),
        'desafioescarpbasico': (BuildContext context) => SlotMachineBasico(),
        'desafioescarpintermedio': (BuildContext context) =>
            SlotMachineIntermedio(),
        'desafioescarpavanzado': (BuildContext context) => SlotMachine(),
        // 'botones': (BuildContext context) => BotonesPage(),
      },
    );
  }
}
