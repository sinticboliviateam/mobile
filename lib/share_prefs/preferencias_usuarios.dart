import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }
  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

// GET y SET del idioma

  get idioma {
    return _prefs.getBool('idioma') ?? true;
  }

  set idioma(bool value) {
    _prefs.setBool('idioma', value);
  }

  // bool _idioma;
  // int _genero;
  // String _nombre;

}
